Shards User Service
=============================

Part of the microservice architecture of the shards backend. Provides simple functionalities for a simple fake user repository. Since this is a stub database, There will be a 4 predefined users. The project runs on port _9012_.


## Technologies ##

The project is build with the [Spring Tool Suite](https://spring.io/tools).

 - [Spring boot](https://projects.spring.io/spring-boot/)
 - [Gradle](https://gradle.org/)
 - [Eureka](https://github.com/netflix/eureka) from the netflix stack

## Getting Started ##

### Installation ###

Please install the following dependancies by hand:

 - [Java](https://www.java.com) (at least 1.7)
 - [Gradle](https://gradle.org/)

All other dependancies should be handled by Gradle.

### Build and Run ###

run the command `gradle bootRun` in your console.

---