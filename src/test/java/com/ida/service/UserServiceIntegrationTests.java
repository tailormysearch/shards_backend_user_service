//package com.ida.service;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.junit.Assert.assertNotNull;
//
//import java.util.List;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import be.ida.model.User;
//import be.ida.repository.UserRepository;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class UserServiceIntegrationTests {
//
//	@Autowired
//	UserRepository userRepository;
//
//	@Before
//	public void setUp() {
//		// NA
//	}
//
//	@Test
//	public void getUserTest() {
//		User user = this.userRepository.getUser(1);
//		assertNotNull(user);
//		assertThat(user.getId() == 1);
//	}
//
//	@Test
//	public void getUsersTest() {
//		List<User> users = this.userRepository.getUsers();
//		assertNotNull(users);
//		assertThat(users.isEmpty() == false);
//	}
//
//}
