//package com.ida;
//
//import static org.hamcrest.CoreMatchers.is;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class UserServiceApplicationTests {
//
//	@Autowired
//	private WebApplicationContext context;
//	private MockMvc mvc;
//
//	@Before
//	public void setUp() {
//		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
//	}
//
//	// Test mappings //
//
//	@Test
//	public void testDefaultUrl() throws Exception {
//		this.mvc.perform(get("/").contentType(MediaType.ALL)).andExpect(status().isOk())
//				.andExpect(content().string(org.hamcrest.Matchers.containsString("Mappings")));
//	}
//
//	@Test
//	public void TestGetUser() throws Exception {
//		this.mvc.perform(get("/user/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
//				.andExpect(jsonPath("$.id", is(1)));
//	}
//
//	@Test
//	public void TestGetUsers() throws Exception {
//		this.mvc.perform(get("/users").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
//	}
//}
