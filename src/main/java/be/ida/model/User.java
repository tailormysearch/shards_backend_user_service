package be.ida.model;

public class User {
	
	private int id;
	private String name;
	private UserType type;

	public User(int id, String name) {
		this.id = id;
		this.name = name;
		this.type = UserType.BASIC;
	}
	
	public User(int id, String name, UserType type) {
		this.id = id;
		this.name = name;
		this.type = type;
	}

	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public UserType getType() {
		return type;
	}

	public void setId(int id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setType(UserType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "User " + id + ": " + name;
	}
}
