package be.ida.model;

public enum UserType {
	BASIC("BasicClient"),
	BIG("bigClient"),
	BUDGET("budgetClient");
	
	private final String name;

	UserType (String name) {
        this.name = name;
    }
	
	@Override
    public String toString() {
       return this.name;
    }
}