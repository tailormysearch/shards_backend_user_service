package be.ida.service;

import java.util.List;

import be.ida.model.User;

public interface UserService {
	public User getUser(int id);

	public List<User> getUsers();
}
