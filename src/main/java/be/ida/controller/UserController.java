package be.ida.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import be.ida.model.User;
import be.ida.service.UserService;

@RestController
public class UserController {
	private final Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private UserService userService;

	@Autowired
	private RequestMappingHandlerMapping requestMappingHandlerMapping;

	@GetMapping("/")
	public String defaultGreeting() {
		String greeting = "<h1>User Service</h1><p>Mappings:</p><ul>";
		for (RequestMappingInfo m : requestMappingHandlerMapping.getHandlerMethods().keySet()) {
			greeting += "<li>" + m + "</li>";
		}
		greeting += "</ul>";
		return greeting;
	}

	@GetMapping("/user/{id}")
	public User getUser(@PathVariable int id) {
		logger.info("Get single product with id:" + id);
		return userService.getUser(id);
	}

	@GetMapping("/users")
	public List<User> getUsers() {
		logger.info("Get all users");
		return userService.getUsers();
	}
}
