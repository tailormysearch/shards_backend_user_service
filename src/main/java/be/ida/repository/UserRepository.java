package be.ida.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import be.ida.model.User;
import be.ida.model.UserType;

/*
 * STUB user repository
 */

@Repository
public class UserRepository {
	List<User> users;

	public UserRepository() {
		users = new ArrayList<User>();

		users.add(new User(1, "David Worth"));
		users.add(new User(2, "Joan Leaven", UserType.BASIC));
		users.add(new User(3, "Hellen Holloway", UserType.BIG));
		users.add(new User(4, "Kazan", UserType.BUDGET));
	}

	public List<User> getUsers() {
		return users;
	}

	public User getUser(int id) {
		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).getId() == id)
				return users.get(i);
		}
		return null;
	}
}
